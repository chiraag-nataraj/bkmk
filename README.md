# bkmk

`bkmk` is a simple bookmark manager (heavily inspired by `pass`) which uses `gnupg` to encrypt your bookmarks and `git` to synchronize them. 

Still in progress. Currently, basic commands (`init`, `add`, `rm`, `mv`, `ls`, `categories`, `show`, and a transparent `git` wrapper) have been implemented.

# How it works

There are several possible commands that can be passed to `bkmk`. All commands with the exception of `ls` will work on a specific category if provided the `-c` option. All commands with the exception of `categories` and `ls` will create the given category if it does not already exist.

* `init` initializes the directory. It defaults to `${HOME}/.local/share/bkmk`. If `-c` is omitted, a file is created for the `default` category - otherwise, a file is created for the provided category. Note that the name of the category is hashed with a salt and the categories and salts are stored in `folders.gpg`. When initializing for the first time, provide your GPG key fingerprints with `-k` (e.g. `bkmk init -c <category> -k fp1 -k fp2 -k fp3` will encrypt to `fp1`, `fp2`, and `fp3`). The GPG fingerprints are stored in `${datadir}/.gpg_id` and loaded from there if it exists to take precedence over `-k` passed as a parameter. As of now, the only way to force re-encryption is by initializing a new bookmark store.
* `add` adds the given url(s) to the given category. to pass multiple urls, simply provide each one as a separate parameter.
* `show` searches for the given strings in the given category. the strings are passed as-is to `grep` as extended regular expressions.
* `rm` removes the urls that match the given strings from the given category. the results (and thus the urls which will be removed) are the same as the results for `show`. hence, it is recommended that the user uses `show` to see which urls will be removed before using `rm` to remove them.
* `mv` renames categories. `-c` selects the _old_ category while the first non-option parameter is the _new_ category. In other words, the syntax is `bkmk mv -c <oldcat> <newcat>`.
* `ls` and `categories` largely do the same thing and display the existing categories, but in different formats.
* `git` passes the rest of the command as-is to `git` and sets the repository to `${datadir}`. run `bkmk git init` to initialize the repo.

# To be added

* Dealing with multiple categories at once
* Hierarchical categories
